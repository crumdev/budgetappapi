namespace BudgetApp.Models
{
    using System;
    public class ExpenseRecord
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public String Category { get; set; }
        public decimal Total { get; set; }
    }
}