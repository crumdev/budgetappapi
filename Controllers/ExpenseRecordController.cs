using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System;
using BudgetApp.Models;

namespace BudgetApp.Controllers
{
    [Route("/api")]
    [ApiController]
    public class ExpenseRecordController : ControllerBase
    {
        private readonly ExpenseRecordContext _context;

        public ExpenseRecordController(ExpenseRecordContext context)
        {
            _context = context;

            if (_context.ExpenseRecords.Count() == 0)
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all TodoItems.
                _context.ExpenseRecords.Add(new ExpenseRecord { Name = "Taco Bell", Date = DateTime.Today, Category="Fast Food",Total = 6.05m });
                _context.ExpenseRecords.Add(new ExpenseRecord { Name = "McDonalds", Date = DateTime.Today, Category="Fast Food",Total = 9.05m });
                _context.ExpenseRecords.Add(new ExpenseRecord { Name = "Walmart", Date = DateTime.Today, Category="Groceries",Total = 106.23m });
                _context.ExpenseRecords.Add(new ExpenseRecord { Name = "Marathon", Date = DateTime.Today, Category="Auto and Gas",Total = 35m });
                _context.SaveChanges();
            }
        }

        [HttpGet("GetAll")]
        public ActionResult<List<ExpenseRecord>> GetAll()
        {
            return _context.ExpenseRecords.ToList();
        }

        [HttpGet("{id}", Name = "GetExpenseRecord")]
        public ActionResult<ExpenseRecord> GetById(long id)
        {
            var item = _context.ExpenseRecords.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }
    }
}