namespace BudgetApp.Models
{
    using Microsoft.EntityFrameworkCore;

    public class ExpenseRecordContext : DbContext
    {
        public ExpenseRecordContext(DbContextOptions<ExpenseRecordContext> options): base(options)
        {
        }

        public DbSet<ExpenseRecord> ExpenseRecords { get; set; }
    }
}